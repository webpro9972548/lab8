import { AppDataSource } from "./data-source"
import { Type } from "./entity/Type";

AppDataSource.initialize()
    .then(async () => {
        const TypesRepository = AppDataSource.getRepository(Type);
        await TypesRepository.clear()
        var type = new Type()
        type.id = 1
        type.name = "drink";
        await TypesRepository.save(type)

        type = new Type()
        type.id = 2
        type.name = "bakery";
        await TypesRepository.save(type)

        type = new Type()
        type.id = 3
        type.name = "food";
        await TypesRepository.save(type)

        const Types = await TypesRepository.find({ order : {id:"ASC"}})
    })
    .catch(error => console.log(error));
