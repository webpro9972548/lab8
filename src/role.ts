import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
    .then(async () => {
        const userRepository = AppDataSource.getRepository(User);
        const rolesRepository = AppDataSource.getRepository(Role);

        await userRepository.clear()
        await rolesRepository.clear()

        var role = new Role()
        role.id = 1
        role.name = "admin";
        await rolesRepository.save(role)

        role = new Role()
        role.id = 2
        role.name = "user";
        await rolesRepository.save(role)

    })
    .catch(error => console.log(error));
